/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function printWelcomeMessages(){
        let fullName = prompt("What is your full name? ");
        console.log("Hello, " + fullName);
    }

    printWelcomeMessages();

    function printAge(){
        let printAge = prompt("How old are you? ");
        console.log("You are " + printAge + " years old.");
    }

    printAge();

    function printAddress(){
        let printAddress = prompt("Enter your address: ");

        console.log("You live in " + printAddress + ".");
    }

    printAddress();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function anime(){
        let name = "Bleach";
        let name2 = "Naruto";
        let name3 = "One Piece";
        let name4 = "Black Clover";
        let name5 = "Zoids";
        console.log("1. " + name);
        console.log("2. " + name2);
        console.log("3. " + name3);
        console.log("4. " + name4);
        console.log("5. " + name5);

    }

    anime();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function movies(){
        let movie1 = "Atlantis: The Lost Empire";
        let rating1 = "54%";
        let movie2 = "Gifted";
        let rating2 = "85%";
        let movie3 = "X-Men";
        let rating3 = "65%";
        let movie4 = "Top Gun";
        let rating4 = "99%";
        let movie5 = "Taken";
        let rating5 = "85%";
        console.log("1. " + movie1);
        console.log("Rotten Tomatoes Rating: " + rating1);
        console.log("2. " + movie2);
        console.log("Rotten Tomatoes Rating: " + rating2);
        console.log("3. " + movie3);
        console.log("Rotten Tomatoes Rating: " + rating3);
        console.log("4. " + movie4);
        console.log("Rotten Tomatoes Rating: " + rating4);
        console.log("5. " + movie5);
        console.log("Rotten Tomatoes Rating: " + rating5);
    }
        movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();


// console.log(friend1);
// console.log(friend2);
// console.log(friend3);